<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Datatables</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css"> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script> -->
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js" type="text/javascript"></script> -->
</head>

<body>
    <div class="container">
        <header>
            <ul class="nav bg-primary">
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">Active</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white disabled" href="#">Disabled</a>
                </li>
            </ul>
        </header>
        <section>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <form id="form-search">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <input type="text" class="form-control" name="name" placeholder="Name">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="email" class="form-control" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="text" class="form-control" name="mobile_phone" placeholder="Phone">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <table id="table-user" class="table-striped table-bordered table-hover" style="width: 100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>City</th>
                                        <th>Created date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {

        function getSearchData() {
            var data = $('#form-search').serializeArray();
            var obj = {};

            $.each(data, function(i, item) {
                if (item.value) {
                    obj[item.name] = item.value;
                }
            });

            return obj;
        }


        var tableUser = $('#table-user').DataTable({
            "processing": true,
            "searching": false,
            "responsive": true,
            "serverSide": true,
            "ajax": {
                "url": "services/user.datatables.php",
                "data": function(data) {
                    return $.extend({}, data, { "cari": getSearchData() });
                }
            },
            "columns": [{
                    "name": "name",
                    "data": 'name',
                },
                {
                    "name": "email",
                    "data": 'email',
                },
                {
                    "name": "mobile_phone",
                    "data": 'mobile_phone',
                },
                {
                    "name": "address",
                    "data": 'address',
                },
                {
                    "name": "created_at",
                    "data": 'created_at',
                },
                {
                    "orderable": false,
                    "data": function(data) {
                        return '<a href="!#" class="button-delete" data-user-id="' + data.user_id + '">Delete</a>';
                    }
                },
            ]
        });


        //Kita hanya perlu melakukan reload table ketika:
        //1. Button Search di submit
        //2. Button reset di tekan
        $(document)
            .on('submit', '#form-search', function(event) {
                event.preventDefault();
                tableUser.ajax.reload();
            })
            .on('click', '#form-search button[type="reset"]', function(event) {
                $('#form-search')[0].reset();
                tableUser.ajax.reload();
            })
            //Register event when delete button is clicked
            .on('click', '.button-delete', function(event) {
                event.preventDefault();
                var userId = $(this).data('user-id');
                swal({
                    text: "Apakah Anda yakin ingin menghapus data ini ?",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "Cancel",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Hapus",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                })
                .then(function(value) {
                  if (!value) return null;

                  $.ajax({
                    url: 'services/user.delete.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {"user_id": userId},
                  })
                  .then(function() {
                    swal("Deleted!", "Success delete user", "success");
                    swal.stopLoading();
                    swal.close();
                  })
                  .catch(function() {
                    swal("Failed!", "Failed delete user!", "error");
                    swal.stopLoading();
                    swal.close();
                  });
                })
            });
    });
    </script>
</body>

</html>