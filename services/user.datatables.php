<?php
require_once('./database.php');


//Lihat dokumentasi berikut ini untuk mengetahui
//Parameter yang dikirim dan dikeluarkan oleh datatables
//https://datatables.net/manual/server-side

/**
 * Order Configuration
 */
$orderColumn = !empty($_GET['order'][0]['column']) ? $_GET['order'][0]['column'] : "created_at";
$orderDirection = !empty($_GET['order'][0]['dir']) ? $_GET['order'][0]['dir'] : "DESC";
$orderQuery = " ORDER BY $orderColumn $orderDirection";

/**
 * Pagination Configuration
 */
$starData = isset($_GET['start']) ? $_GET['start'] : 0;
$pageSize = isset($_GET['length']) ? $_GET['length'] : 10;
$limitQuery = " LIMIT $starData , $pageSize ";

/**
 * Filter pencarian
 * Parameter "cari" ada di javascript saat define datatables
 * di dalam options ajax > data
 */
$keyWord = ! empty($_GET['cari']) ? $_GET['cari'] : [];
$condition = [];
$whereQuery = "";
foreach ($keyWord as $key => $value) {
	if($value !== ""){
		$condition[] = " $key like '%$value%' ";
	}
}

if(!empty($condition)){
	$whereQuery = " WHERE ";
	foreach ($condition as $index => $value) {
			$whereQuery .= $value;
			if($index+1 !== count($condition)){
				$whereQuery.=" AND ";
			}
	}	
}


$data = [];
$recordsTotal = 0;

$mainQuery = "SELECT * FROM users ". $whereQuery . $orderQuery . $limitQuery;
$totalQuery = "SELECT count(*) FROM users ". $whereQuery;

if($result = $mysqli->query($mainQuery)){
  while ($row = $result->fetch_object()) {
     $data[] = $row;
  }

  $result->close();
}else{
	die($mysqli->error);
}

if($countResult = $mysqli->query($totalQuery)){
  while ($row = $countResult->fetch_row()) {
     $recordsTotal = current($row);
  }
	/* free result set */
  $countResult->close();
}else{
	die($mysqli->error);
}

/* close connection */
$mysqli->close();

header('Content-Type: application/json');

echo json_encode([
	'data'=> $data,
	'recordsTotal' => $recordsTotal,
  "recordsFiltered" => $recordsTotal,
]);